extends Node3D

@export var player : CharacterBody3D

var is_seated : bool = false

@export var sit_point : Node3D
@export var standing_point : Node3D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func interact():
	if is_seated == true:
		player.can_move = true
		player.position = standing_point.global_position
		is_seated = false
	else:
		player.can_move = false
		player.position = sit_point.global_position
		player.rotation = sit_point.global_rotation
		is_seated = true	
