extends MeshInstance3D

var is_on : bool = false

@onready var light : MeshInstance3D = $MeshInstance3D
@onready var audio_player : AudioStreamPlayer3D = $AudioStreamPlayer3D

@export var red : StandardMaterial3D
@export var green : StandardMaterial3D

@export var bip : AudioStream
@export var ac : AudioStream

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func toggle_use():
	if is_on == true:
		light.material_override = red
		Manager.ac_on = false
		Manager.devices_on -= 1
		audio_player.stream = bip
		audio_player.play()
		is_on = false
	else:
		if Manager.avalible_power > 0: 
			light.material_override = green
			Manager.ac_on = true
			Manager.devices_on += 1
			audio_player.play()
			is_on = true	
		


func _on_audio_stream_player_3d_finished():
	if is_on == true:
		audio_player.stream = ac
		audio_player.play()
