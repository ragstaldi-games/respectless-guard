extends MeshInstance3D

@export var light : SpotLight3D

var is_on : bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func interact():
	if is_on == true:
		light.visible = false
		is_on = false
		Manager.devices_on -= 1
	else:
		if Manager.avalible_power > 0:
			light.visible = true
			is_on = true
			Manager.devices_on += 1 	
