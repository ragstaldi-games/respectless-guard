extends CharacterBody3D

@export var ac : MeshInstance3D
@export var normal : StandardMaterial3D
@export var heated : StandardMaterial3D

@onready var beer_pos : Node3D = $beer_pos

var can_interact : bool = false
var current_object
var beer : RigidBody3D

var axis : Vector2
var speed = 2
var accel = 10

var heat = 36
var alcohol = 0

var can_move : bool  = true


func _ready():
	pass 


func _process(delta):
	if getAxis() != Vector2.ZERO:
		look_at(position + Vector3(-velocity.x,velocity.y,-velocity.z))
		
	if heat >= 38:
		get_child(0).material_override = heated
	elif heat < 38:
		get_child(0).material_override = normal	
		
	
	if heat >= 45:
		blackout()
		
	

func _physics_process(delta):
	if Manager.game_started == true && can_move == true:
		playerMovement(delta)
	
		
	

func getAxis() -> Vector2:
	axis.x = int(Input.is_action_pressed("Down")) - int(Input.is_action_pressed("Up"))
	axis.y = int(Input.is_action_pressed("Right")) - int(Input.is_action_pressed("Left"))
	
	return axis
	

	
func playerMovement(delta):
	#velocity.x = getAxis().x * speed
	#velocity.z = getAxis().y * speed
	
	var direction = Vector3(getAxis().x,0,-getAxis().y)
	
	direction = direction.normalized()
	
	velocity = velocity.lerp(direction * speed,accel*delta)
	
	move_and_slide()

func _input(event):
	if event.is_action_pressed("Interact") && can_interact == true:
		current_object.interact()
		
	if event.is_action_pressed("AC"):
		ac.toggle_use()
		
	if event.is_action_pressed("Drink") && beer != null:
		heat -= 3
		beer.drink()
		beer = null		

func blackout():
	heat = 36
	get_parent().get_node("Game_manager").blackout()



	
	



func _on_detector_body_entered(body):
	if body.get_parent().is_in_group("prop"):
		current_object = body.get_parent()
		can_interact = true
	


func _on_detector_body_exited(body):
	current_object = null
	can_interact = false


func _on_timer_timeout():
	if Manager.ac_on == false:
		heat += int(Manager.humidity / Manager.temperature)
	else:
		heat = 36	
	
