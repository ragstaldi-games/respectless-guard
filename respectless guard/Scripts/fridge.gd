extends Node3D

@export var player : CharacterBody3D

@onready var ref_pos = $Node3D

var beer = preload("res://Nodes/beer.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func interact():
	var instance = beer.instantiate()
	player.beer_pos.add_child(instance)
	player.beer = instance
