extends MarginContainer

@onready var text : Label = $Label
@onready var timer : Timer = $Timer
@onready var audio : AudioStreamPlayer3D = $AudioStreamPlayer3D

# Called when the node enters the scene tree for the first time.
func _ready():
	timer.start()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Manager.time > 6 && Manager.time < 8:
		text.text = "7:00"
	else:
		text.text = ""	


func _on_timer_timeout():
	if audio.playing == false && Manager.game_started == false && Manager.time > 6 && Manager.time < 8:
		audio.play()
		


func _on_audio_stream_player_3d_finished():
	hide()
	Manager.game_started = true
