extends Node3D

@onready var video_player : VideoStreamPlayer = $Screen/SubViewport/SubViewportContainer/VideoStreamPlayer

var video_path1 = "res://Assets/Masterchef en cuarentona.ogg"
var video_path2 = "res://Assets/Suero de la Facha.ogg"
var video_path3 = "res://Assets/Tutoriales de mierda - como usar una licuadora.ogg"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func interact():
	if Manager.avalible_power > 0:
		var random_video = randi() % 3
	
		if random_video == 0:
			video_player.stream.file = video_path1
		elif random_video == 1:
			video_player.stream.file = video_path2
		else:
			video_player.stream.file = video_path3
		
		video_player.play()
	
		Manager.devices_on += 1
		 		
