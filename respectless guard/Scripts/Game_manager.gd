extends Node


@onready var timer : Timer = $Timer
@onready var clock : MarginContainer = $"../MarginContainer"
@onready var enviroment : WorldEnvironment = $"../WorldEnvironment"



func _ready():
	timer.start()


func _process(delta):
	Manager.time += delta / 10
	
	if Manager.time > 24:
		blackout()
		Manager.days += 1
		
		
	if Manager.time >= 0 && Manager.time <= 5:
		lerp(enviroment.environment.background_energy_multiplier, 0.2, 1)		
	elif Manager.time > 5 && Manager.time < 10:
		lerp(enviroment.environment.background_energy_multiplier, 0.5, 1)
	elif Manager.time > 10 && Manager.time < 12:
		lerp(enviroment.environment.background_energy_multiplier, 0.7, 1)
	elif Manager.time > 12 && Manager.time < 16:
		lerp(enviroment.environment.background_energy_multiplier, 1.0, 1)
	elif Manager.time > 16 && Manager.time < 20:
		lerp(enviroment.environment.background_energy_multiplier, 0.6, 1)
	elif Manager.time > 20 && Manager.time < 24:
		lerp(enviroment.environment.background_energy_multiplier, 0.4, 1)
	
	
	if Manager.days == 1:
		setManagerValues(30,30,100)
	elif Manager.days == 2:
		setManagerValues(32,70,45)
	elif Manager.days == 3:
		setManagerValues(35,85,60)
	else:
		setManagerValues(40,100,20)			
						
	if Manager.avalible_power <= 0:
		Manager.power_on = false

func blackout():
	Manager.game_started = false
	Manager.time = 7.0
	clock.show()	
	
func setManagerValues(temperature, humidity, power):
	Manager.temperature = temperature
	Manager.humidity = humidity
	Manager.avalible_power = power



func _on_timer_timeout():
	if Manager.power_on == true:
		if Manager.devices_on == 1:
			Manager.avalible_power -= 2
		elif Manager.devices_on > 1 && Manager.devices_on <= 3:
			Manager.avalible_power -= 5
		elif Manager.devices_on > 3 && Manager.devices_on <= 5:
			Manager.avalible_power -= 7
		else :
			Manager.avalible_power -= 10			
	
