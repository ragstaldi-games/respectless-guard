extends Area3D

@export var stage : int


# Called when the node enters the scene tree for the first time.
func _ready():
	body_entered.connect(_on_player_enter)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_player_enter(body):
	if body.name == "Player":
		Manager.stage = stage
	
