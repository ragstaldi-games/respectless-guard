extends Node3D

@export var player : CharacterBody3D

var is_seated : bool = false

@export var sit_point : Node3D
@export var standing_point : Node3D

@onready var water = $Water

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func interact():
	if is_seated == true:
		player.can_move = true
		player.position = standing_point.global_position
		player.rotation = standing_point.rotation
		Manager.ac_on = false
		water.hide()
		is_seated = false
	else:
		player.can_move = false
		player.position = sit_point.global_position
		player.rotation = Vector3(-60,0,0)
		Manager.ac_on = true
		water.show()
		is_seated = true	
