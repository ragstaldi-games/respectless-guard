extends Node3D

@onready var camera_array = [$Camera1,$Camera2,$Camera3,$Camera4,$Camera5,$Camera6,$Camera7]

var current_camera : Camera3D

# Called when the node enters the scene tree for the first time.
func _ready():
	camera_array[0].make_current()
	current_camera = camera_array[0]


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	current_camera = camera_array[Manager.stage]
	current_camera.make_current()
	
	
