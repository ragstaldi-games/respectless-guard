extends CharacterBody3D

enum Objective{
	FOOD,
	CLOTHES
}

enum Mood{
	HAPPY,
	NORMAL,
	ANGRY
}

@onready var nav : NavigationAgent3D = $NavigationAgent3D
@onready var timer : Timer = $Timer
@onready var mood_display : Sprite3D = $Mood

@export var happy : Texture2D
@export var normal : Texture2D
@export var angry : Texture2D

var target : Node3D
var current_door

var objective : Objective
var mood : Mood

var objective_compleated : bool = false
var stop : bool = false

var speed = 2
var accel = 10

var patience = 5

# Called when the node enters the scene tree for the first time.
func _ready():
	objective = randi() & 2
	if objective == Objective.FOOD:
		target = get_parent().get_node("Food")
	else:
		target = get_parent().get_node("Shop")



func _process(delta):
	look_at(position + Vector3(-velocity.x,velocity.y,-velocity.z))

	
	
func _physics_process(delta):	
	if stop == false:
		var direction = Vector3()
	
		nav.target_position = target.global_position
	
		direction = nav.get_next_path_position() - global_position
		direction = direction.normalized()	
	
		velocity = velocity.lerp(direction * speed, accel * delta)
	
		move_and_slide()

func interact_with_door(door):
	current_door = door
	stop = true
	timer.start()
	
func change_target(target):
	pass	


func _on_timer_timeout():
	patience -= 1
	
	if stop == false:
		timer.stop()
	
	if patience >= 5:
		mood = Mood.HAPPY
		mood_display.texture = happy
	elif patience > 1 && patience < 5:
		mood = Mood.NORMAL
		mood_display.texture = normal
	else:
		mood = Mood.ANGRY
		mood_display.texture = angry
		stop = false
		current_door.interact()	
		timer.stop()		
	


func _on_area_3d_body_entered(body):
	pass # Replace with function body.
